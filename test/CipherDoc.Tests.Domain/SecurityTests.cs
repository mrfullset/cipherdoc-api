using CipherDoc.Domain.Core;
using CipherDoc.Domain.Security;

namespace CipherDoc.Tests.Domain;

public class SecurityTests
{
	[Theory]
	[InlineData(null)]
	[InlineData("")]
	public void Ed25519Key_Should_Not_BeNullOrEmpty(string strKey)
	{
		var result = Ed25519PublicKey.Create(strKey!);
		Assert.True(result.IsFailure);
	}

	[Fact]
	public void Ed25519Key_Should_Be32BytesEncodedWithBase64()
	{
		const string strKey = "QbbVUXfM25FfOwA4KmDebw4x8L9bvmC519XjkYroLjY=";

		var result = Ed25519PublicKey.Create(strKey);

		Assert.True(result.IsSuccess);
	}


	[Fact]
	public void Ed25519Key_Should_BeWellFormed()
	{
		const string strKey = "somebadstring";

		var result = Ed25519PublicKey.Create(strKey);

		Assert.True(result.IsFailure);
	}

	[Theory]
	[InlineData(null, null)]
	[InlineData("", "")]
	[InlineData(null, "")]
	[InlineData("", null)]
	public void Aes256EncryptedKey_Should_Not_BeNullOrEmpty(string strKey, string strNonce)
	{
		var result = Aes256EncryptedKey.Create(strKey, strNonce);
		Assert.True(result.IsFailure);
	}

	[Fact]
	public void Aes256EncryptedKey_Should_Be256BytesEncodedWithBase64()
	{
		const string strKey = "2VUCBlrG4Zg7kqO11PXplFyuNSUPRL4IwXrDOWwpYnuK"
			+ "260j2f3ETS3ZWaHk+2y4vmsiBpCQSK/KNwbFtnOaEJMrtoyY4XJ9MzHuMGt4"
			+ "CAyALj53694V9kt2lUQkS1xoGkcc4fY0JG9Ak15Z0RodcnpIPrK49YTCMDf2"
			+ "A8nmhQXBF9CK5N5M2O02v1J/41uwGNBeIETYX5faD9+V67Dg7EBBjEjEpZrE"
			+ "BgapbaPZQxok2fHHr/PSGEj80MZ99e+9cIGJaDWHVPWmO1I6UOILa09q0plK"
			+ "V0BULAHRW9IGbQEbf5QDgPZVWRB8YvzFhwY9oWXga6+oHBXUdSSMi6xIw7X0"
			+ "wHwmLtuzYNVOVudtNpY=";

		const string strNonce = "FyRfN9nypmWwE+snYtSghRVvi1oJdmAY";

		var result = Aes256EncryptedKey.Create(strKey, strNonce);

		Assert.True(result.IsSuccess);
	}

	[Theory]
	[InlineData("ZsloTRBK0ZoxZIYP1TfhVEHNVsoWKfW14ZcK0oXjhZiAQdADreR0/"
		+ "bfYCNzyf00Yx2k4MtN2BgVHaodiqF8+vY5If/veh94oo2fvfuRFdIJFNNayYmKMyx"
		+ "a1EcWeT7F/CqanSaOaoI+79kIjg0WU/hBFciOe0F67v99DMJylnrhfWXXKf56M6S6"
		+ "ZFvCa1x2SFogQxgyaZVVOvGn5aYyv6s1NtxYB5kFaQ5OMYcRGpulKIHFE5r72AZNO"
		+ "irldmt2jRLb7lKGqh6I=", "FyRfN9nypmWwE+snYtSghRVvi1oJdmAY")]
	[InlineData("2VUCBlrG4Zg7kqO11PXplFyuNSUPRL4IwXrDOWwpYnuK"
		+ "260j2f3ETS3ZWaHk+2y4vmsiBpCQSK/KNwbFtnOaEJMrtoyY4XJ9MzHuMGt4"
		+ "CAyALj53694V9kt2lUQkS1xoGkcc4fY0JG9Ak15Z0RodcnpIPrK49YTCMDf2"
		+ "A8nmhQXBF9CK5N5M2O02v1J/41uwGNBeIETYX5faD9+V67Dg7EBBjEjEpZrE"
		+ "BgapbaPZQxok2fHHr/PSGEj80MZ99e+9cIGJaDWHVPWmO1I6UOILa09q0plK"
		+ "V0BULAHRW9IGbQEbf5QDgPZVWRB8YvzFhwY9oWXga6+oHBXUdSSMi6xIw7X0"
		+ "wHwmLtuzYNVOVudtNpY=", "B/0dgcoRUuf0Me7ZjE7Qew==")]
	public void Aes256EncryptedKey_Should_ReturnHasInvalidKeyLength_If_InputIHasInvalidLength(string strKey, string strNonce)
	{
		var result = Aes256EncryptedKey.Create(strKey, strNonce);

		Assert.True(result.IsFailure);
		Assert.Equal(DomainErrors.Aes256EncryptedKey.HasInvalidKeyLength, result.Error);
	}

	[Theory]
	[InlineData("2VUCBlrG4Zg7kqO11PXplFyuNSUPRL4IwXrDOWwpYnuK"
		+ "260j2f3ETS3ZWaHk+2y4vmsiBpCQSK/KNwbFtnOaEJMrtoyY4XJ9MzHuMGt4"
		+ "CAyALj53694V9kt2lUQkS1xoGkcc4fY0JG9Ak15Z0RodcnpIPrK49YTCMDf2"
		+ "A8nmhQXBF9CK5N5M2O02v1J/41uwGNBeIETYX5faD9+V67Dg7EBBjEjEpZrE"
		+ "BgapbaPZQxok2fHHr/PSGEj80MZ99e+9cIGJaDWHVPWmO1I6UOILa09q0plK"
		+ "V0BULAHRW9IGbQEbf5QDgPZVWRB8YvzFhwY9oWXga6+oHBXUdSSMi6xIw7X0"
		+ "wHwmLtuzYNVOVudtNpY=", "somebadstring")]
	[InlineData("somebadstring", "FyRfN9nypmWwE+snYtSghRVvi1oJdmAY")]
	public void Aes256EncryptedKey_Should_ReturnNotWellFormedBase64Error_If_InputIsNotWellFormed(string strKey, string strNonce)
	{
		var result = Aes256EncryptedKey.Create(strKey, strNonce);

		Assert.True(result.IsFailure);
		Assert.Equal(DomainErrors.Aes256EncryptedKey.IsNotWellFormedBase64, result.Error);
	}

	[Theory]
	[InlineData(null)]
	[InlineData("")]
	public void Pbkdf2Hash_Value_ShouldNotBeNullOrEmpty(string value)
	{
		var result = Pbkdf2Hash.Create(value);
		Assert.True(result.IsFailure);
		Assert.Equal(result.Error, DomainErrors.Generic.IsNullOrEmpty);
	}

	[Theory]
	[InlineData("1959550c0d6d57cd43a090deb92d8b2fea613398fd357fc75e22c08488e95365")]
	[InlineData("d7e6d0416adc9af5d1b450ee2812e7ac3d4ac31435ff769f3add12edea660b23")]
	public void Pbkdf2Hash_Value_ShouldBeAes256String(string value)
	{
		var result = Pbkdf2Hash.Create(value);
		Assert.True(result.IsSuccess);
	}

	[Theory]
	[InlineData("1959550c0d6d57cd43a090dea613398fd357fc75e22c08488e95365")]
	[InlineData("d7e6d0416a2edea660b23")]
	public void Pbkdf2Hash_Create_ShouldReturnInvalidLengthError_IfValueIsNot64Chars(string value)
	{
		var result = Pbkdf2Hash.Create(value);

		Assert.True(result.IsFailure);
		Assert.Equal(result.Error, DomainErrors.Pbkdf2Hash.HasInvalidHashLength);
	}

	[Theory]
	[InlineData("1959550c0d6d57cd43a090deb92d8b2fea613398fd357fc75e22c08488e95365")]
	[InlineData("d7e6d0416adc9af5d1b450ee2812e7ac3d4ac31435ff769f3add12edea660b23")]
	public void Pbkdf2Hash_Create_ShouldGenerateSalt(string value)
	{
		var result = Pbkdf2Hash.Create(value);

		Assert.True(result.IsSuccess);

		var salt = result.Value.Salt;
		Assert.NotEmpty(salt);
	}

	[Theory]
	[InlineData("1959550c0d6d57cd43a090deb92d8b2fea613398fd357fc75e22c08488e95365")]
	[InlineData("d7e6d0416adc9af5d1b450ee2812e7ac3d4ac31435ff769f3add12edea660b23")]
	public void Pbkdf2Hash_Value_ShouldBe256BitsLong(string value)
	{
		var hash = Pbkdf2Hash.Create(value).Value;

		var hashBytes = Convert.FromBase64String(hash.Value);

		Assert.True(hashBytes.Length == 256);
	}

	[Fact]
	public void Pbkdf2Hash_Should_CheckAgainstClientHash()
	{
		const string clientHash = "1959550c0d6d57cd43a090deb92d8b2fea613398fd357fc75e22c08488e95365";
		var initialPbk2dfHash = Pbkdf2Hash.Create(clientHash).Value;

		var result = initialPbk2dfHash.EqualsToClientHash(clientHash);

		Assert.True(result);
	}

	[Fact]
	public void Pbkdf2Hash_Should_FailCheckAgainstInvalidClientHash()
	{
		const string initialClientHash = "1959550c0d6d57cd43a090deb92d8b2fea613398fd357fc75e22c08488e95365";
		var initialPbk2dfHash = Pbkdf2Hash.Create(initialClientHash).Value;

		const string invalidClientHash = "1959550c0d6d57cd43a090deb92d8b2fea613398fd357fc75e22c08488e95366";
		var result = initialPbk2dfHash.EqualsToClientHash(invalidClientHash);

		Assert.False(result);
	}

	[Theory]
	[InlineData(null)]
	[InlineData("")]
	public void ActivationSalt_Should_NotBeNullOrEmpty(string strSalt)
	{
		var result = ActivationSalt.Create(strSalt!);
		Assert.True(result.IsFailure);
	}

	[Fact]
	public void ActivationSalt_Should_Be128BytesEncodedWithBase64()
	{
		const string strSalt = "+lfrcJXYXEzLrd8ilm5MT2F5hdk4vh3KFb93KaiJArCtz9FjyrdDMKtbMPDKkGxH49wPC2z17qfQalZ90kc9RlTz/V2w9Ttq5INKaOg8Vzx+4rq83lfKXtTJ6dJ0xJGOnX9e+lc38HgijuVSGHRnwiEoyv50qrJng+rYkgzYHxQ=";

		var result = ActivationSalt.Create(strSalt);

		Assert.True(result.IsSuccess);
	}

	[Fact]
	public void ActivationSalt_Should_BeWellFormed()
	{
		const string strSalt = "+lfrcJXYXEzLrd8ilm5MT2F5hdk4vh3KFb93KaiJArCtz9FjyrdDMKtbMPDKkGxH49wPC2z17qfQalZ90kc9RlTz/V2w9Ttq5INKaOg8Vzx+4rq83lfKXtTJ6dJ0xJGOnX9e+lc38HgijuVSGHRnwiEoyv50qrJng+rYkgzYHxQ=";

		var result = ActivationSalt.Create(strSalt);

		Assert.True(result.IsSuccess);
	}

	[Theory]
	[InlineData("P4bpIO387p7yX10KGeZsbPvLd/FGCUSubkFh4cAeKcJBUepNAhxYwCtbVt00X18kXE+Meg3kdw+D8foOuZ4TkdNVufIwnHA94EoV+9E0uUn5Oze9CsKahL2gXDM3iqgga6v1tLRYF6U6jiKD3cwBcnPE+gBHUfTdXAUMLWT/YQ==")]
	[InlineData("Vj2/n3ijQq1C0ap4v5qjSqJc1x1KdNnA/VZSBfNmq/A=")]
	public void ActivationSalt_Should_ReturnInvalidLength_If_InputHasInvalidLength(string strSalt)
	{
		var result = ActivationSalt.Create(strSalt);

		Assert.True(result.IsFailure);
		Assert.Equal(result.Error, DomainErrors.ActivationSalt.HasInvalidKeyLength);
	}

	[Fact]
	public void ActivationSalt_Should_ReturnNotWellFormed_If_InputIsNotWellFormed()
	{
		const string strSalt = "somebadstring";

		var result = ActivationSalt.Create(strSalt);
		
		Assert.True(result.IsFailure);
		Assert.Equal(result.Error, DomainErrors.ActivationSalt.IsNotWellFormedBase64);
	}

}
