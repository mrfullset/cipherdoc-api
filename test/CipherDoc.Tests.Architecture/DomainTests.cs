using NetArchTest.Rules;

namespace CipherDoc.Tests.Architecture;

public class DomainTests
{
	// Do not forget:
	//
	// Arrange
	// Act
	// Assert

	[Fact]
	public void Domain_Should_Not_HaveDependencyOnOtherProjects()
	{
		var assembly = typeof(Domain.AssemblyReference).Assembly;

		var otherProjects = new[]
		{
			ProjectNamespaces.Infrastructure, ProjectNamespaces.Application, ProjectNamespaces.WebApi
		};

		var result = Types.InAssembly(assembly)
			.ShouldNot()
			.HaveDependencyOnAll(otherProjects)
			.GetResult();

		Assert.True(result.IsSuccessful);
	}
}
