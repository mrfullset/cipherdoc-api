using NetArchTest.Rules;

namespace CipherDoc.Tests.Architecture;

public class WebApiTests
{
	// Do not forget:
	//
	// Arrange
	// Act
	// Assert

	[Fact]
	public void Controllers_Should_HaveDependencyOnMediatR()
	{
		var assembly = typeof(WebAPI.AssemblyReference).Assembly;

		var result = Types.InAssembly(assembly)
			.That()
			.HaveNameEndingWith("Controller")
			.Should()
			.HaveDependencyOn("MediatR")
			.GetResult();

		Assert.True(result.IsSuccessful);
	}
}
