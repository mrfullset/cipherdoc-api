using NetArchTest.Rules;

namespace CipherDoc.Tests.Architecture;

public class ApplicationTests
{
	// Do not forget:
	//
	// Arrange
	// Act
	// Assert

	[Fact]
	public void Application_Should_Not_HaveDependencyOnOtherProjects()
	{
		var assembly = typeof(Application.AssemblyReference).Assembly;

		var otherProjects = new[]
		{
			ProjectNamespaces.Infrastructure, ProjectNamespaces.WebApi
		};

		var result = Types.InAssembly(assembly)
			.ShouldNot()
			.HaveDependencyOnAll(otherProjects)
			.GetResult();

		Assert.True(result.IsSuccessful);
	}

	[Fact]
	public void Handlers_Should_HaveDependencyOnDomain()
	{
		var assembly = typeof(Application.AssemblyReference).Assembly;

		var result = Types.InAssembly(assembly)
			.That()
			.HaveNameEndingWith("Handler")
			.And()
			.DoNotHaveNameMatching("TestCommandHandler")
			.Should()
			.HaveDependencyOn(ProjectNamespaces.Domain)
			.GetResult();

		Assert.True(result.IsSuccessful);
	}
}
