namespace CipherDoc.Tests.Architecture;

public static class ProjectNamespaces
{
	public const string Domain = "CipherDoc.Domain";
	public const string Application = "CipherDoc.Application";
	public const string Infrastructure = "CipherDoc.Infrastructure";
	public const string WebApi = "CipherDoc.WebAPI";
}
