using NetArchTest.Rules;

namespace CipherDoc.Tests.Architecture;

public class InfrastructureTests
{
	// Do not forget:
	//
	// Arrange
	// Act
	// Assert

	[Fact]
	public void Infrastructure_Should_Not_HaveDependencyOnOtherProjects()
	{
		var assembly = typeof(Infrastructure.AssemblyReference).Assembly;

		var otherProjects = new[]
		{
			ProjectNamespaces.WebApi
		};

		var result = Types.InAssembly(assembly)
			.ShouldNot()
			.HaveDependencyOnAll(otherProjects)
			.GetResult();

		Assert.True(result.IsSuccessful);
	}
}
