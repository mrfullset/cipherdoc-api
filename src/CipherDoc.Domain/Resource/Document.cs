using CipherDoc.Domain.Security;

namespace CipherDoc.Domain.Resource;

public sealed class Document : Resource
{
	public Guid FileName { get; private set; }
	public EncryptedDocumentKey Key { get; private set; }

	private Document() {}

	private Document(int id, string name, Guid fileName, EncryptedDocumentKey key) : base(id, name)
	{
		Key = key;
		FileName = fileName;
	}

	public static Document Create(string name, EncryptedDocumentKey key)
	{
		return new Document(default, name, Guid.NewGuid(), key);
	}
}
