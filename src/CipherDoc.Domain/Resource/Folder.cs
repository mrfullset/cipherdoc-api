namespace CipherDoc.Domain.Resource;

public class Folder : Resource
{
	private readonly IList<Document> _documents;

	public IReadOnlyList<Document> Documents => _documents.AsReadOnly();

	public const string DefaultName = "default";

	private Folder() {}

	private Folder(string name, IList<Document> documents) : base(name)
	{
		_documents = documents;
	}

	public static Folder Create(string name)
	{
		return new Folder(name, new List<Document>());
	}

	public static Folder CreateDefault() => Create(DefaultName);
}
