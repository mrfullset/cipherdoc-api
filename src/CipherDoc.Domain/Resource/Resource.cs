using CipherDoc.Domain.Core.Primitives;

namespace CipherDoc.Domain.Resource;

public abstract class Resource : AggregateRoot
{
	public string Name { get; protected set; }

	protected Resource() {}

	protected Resource(string name)
	{
		Name = name;
	}

	protected Resource(int id, string name) : base(id)
	{
		Name = name;
	}
}
