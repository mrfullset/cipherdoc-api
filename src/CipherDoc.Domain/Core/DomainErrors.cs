using CipherDoc.Domain.Core.Primitives;

namespace CipherDoc.Domain.Core;

public static class DomainErrors
{
	public static class Ed25519PublicKey
	{
		public static readonly Error IsNotWellFormedBase64
			= new Error(nameof(Ed25519PublicKey) + nameof(IsNotWellFormedBase64), "Key is not well formed base64");

		public static readonly Error HasInvalidKeyLength
			= new Error(nameof(Ed25519PublicKey) + nameof(HasInvalidKeyLength), "Key has invalid length");
	}

	public static class ActivationSalt
	{
		public static readonly Error IsNotWellFormedBase64
			= new Error(nameof(ActivationSalt) + nameof(IsNotWellFormedBase64), "Salt is not well formed base64");

		public static readonly Error HasInvalidKeyLength
			= new Error(nameof(ActivationSalt) + nameof(HasInvalidKeyLength), "Salt has invalid length");
	}

	public static class Aes256EncryptedKey
	{
		public static readonly Error IsNotWellFormedBase64
			= new Error(nameof(Aes256EncryptedKey) + nameof(IsNotWellFormedBase64), "Key is not well formed base64");

		public static readonly Error HasInvalidKeyLength
			= new Error(nameof(Aes256EncryptedKey) + nameof(HasInvalidKeyLength), "Key has invalid length");
	}

	public static class Pbkdf2Hash
	{
		public static readonly Error HasInvalidHashLength
			= new Error(nameof(Pbkdf2Hash) + nameof(HasInvalidHashLength), "Hash has invalid length");
	}

	public static class User
	{
		public static readonly Error LoginAlreadyExists
			= new Error(nameof(User) + nameof(LoginAlreadyExists), "User with such login already exists");
	}

	public static class Generic
	{
		public static readonly Error IsNullOrEmpty
			= new Error(nameof(Generic) + nameof(IsNullOrEmpty), "Value is null or empty");
	}
}
