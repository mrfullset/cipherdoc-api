namespace CipherDoc.Domain.Core.Primitives;

public abstract class Entity : IEquatable<Entity>
{
	/// <summary>
	/// Initializes a new instance of the <see cref="Entity"/> class.
	/// </summary>
	/// <param name="id">The entity identifier.</param>
	protected Entity(int id)
		: this()
	{
		if (IsIdTransient(id))
		{
			throw new ArgumentException("Using non transient constructor with transient id");
		}
		Id = id;
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="Entity"/> class.
	/// </summary>
	/// <remarks>
	/// Required by EF Core.
	/// </remarks>
	protected Entity()
	{
	}

	/// <summary>
	/// Gets or sets the entity identifier.
	/// </summary>
	public int Id { get; }

	public bool IsTransient() => IsIdTransient(Id);

	private static bool IsIdTransient(int id) => id == default;

	public static bool operator ==(Entity? a, Entity? b)
	{
		if (a is null && b is null)
		{
			return true;
		}

		if (a is null || b is null)
		{
			return false;
		}

		return a.Equals(b);
	}

	public static bool operator !=(Entity a, Entity b) => !(a == b);

	/// <inheritdoc />
	public bool Equals(Entity? other)
	{
		if (other is null)
		{
			return false;
		}

		return ReferenceEquals(this, other) || Id == other.Id;
	}

	/// <inheritdoc />
	public override bool Equals(object? obj)
	{
		if (obj is null)
		{
			return false;
		}

		if (ReferenceEquals(this, obj))
		{
			return true;
		}

		if (obj.GetType() != GetType())
		{
			return false;
		}

		if (obj is not Entity other)
		{
			return false;
		}

		if (IsTransient() || IsIdTransient(other.Id))
		{
			return false;
		}

		return Id == other.Id;
	}

	public override int GetHashCode() => Id.GetHashCode();
}
