using CipherDoc.Domain.Resource;
using CipherDoc.Domain.Security;

namespace CipherDoc.Domain.User;

public sealed class Client : User
{
	private readonly IList<Folder> _folders;
	private Pbkdf2Hash _hashedPassword;

	public IReadOnlyList<Folder> Folders => _folders.AsReadOnly();

	public Folder DefaultFolder => _folders.FirstOrDefault(f => f.Name == Folder.DefaultName)!;

	private Client() {}

	private Client(string login, Pbkdf2Hash hashedPassword, IList<Folder> folders, IList<int> foreignResourceIds, PublicKey publicKey)
		: base(login, foreignResourceIds, publicKey)
	{
		_hashedPassword = hashedPassword;
		_folders = folders;
	}

	public bool IsValidPasswordHash(string hash)
		=> _hashedPassword.EqualsToClientHash(hash);

	public static Client Create(string login, Pbkdf2Hash hashedPassword, PublicKey publicKey)
	{
		var folders = new List<Folder>()
		{
			Folder.CreateDefault()
		};
		return new Client(login, hashedPassword, folders, new List<int>(), publicKey);
	}
}
