using CipherDoc.Domain.Core.Primitives;
using CipherDoc.Domain.Security;

namespace CipherDoc.Domain.User;

public abstract class User : AggregateRoot
{
	private readonly IList<int> _foreignResourceIds;

	public string Login { get; private set; }

	public PublicKey PublicKey { get; private set; }

	protected User() {}

	protected User(string login, IList<int> foreignResourceIds, PublicKey publicKey)
	{
		Login = login;
		_foreignResourceIds = foreignResourceIds;
		PublicKey = publicKey;
	}
}
