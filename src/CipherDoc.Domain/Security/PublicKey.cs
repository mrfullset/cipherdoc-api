using CipherDoc.Domain.Core.Primitives;

namespace CipherDoc.Domain.Security;

public sealed class PublicKey : Entity
{
	public Ed25519PublicKey Key { get; private set; }
	
	public ActivationSalt ActivationSalt { get; private set; }

	private PublicKey() {}

	private PublicKey(Ed25519PublicKey key, ActivationSalt activationSalt)
	{
		Key = key;
		ActivationSalt = activationSalt;
	}

	public static PublicKey Create(Ed25519PublicKey key, ActivationSalt activationSalt)
	{
		return new PublicKey(key, activationSalt);
	}
}
