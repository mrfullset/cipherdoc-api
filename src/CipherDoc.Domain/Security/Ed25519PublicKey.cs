using CipherDoc.Domain.Core;
using CipherDoc.Domain.Core.Primitives;
using CipherDoc.Domain.Core.Primitives.Result;

namespace CipherDoc.Domain.Security;

public class Ed25519PublicKey : ValueObject
{
	private const int ValueLength = 32;
	
	public string Value { get; }

	private Ed25519PublicKey(string value)
	{
		Value = value;
	}

	public static Result<Ed25519PublicKey> Create(string value)
	{
		if (string.IsNullOrEmpty(value))
		{
			return Result.Failure<Ed25519PublicKey>(DomainErrors.Generic.IsNullOrEmpty);
		}

		return ParseBase64Key(value);
	}

	private static Result<Ed25519PublicKey> ParseBase64Key(string value)
	{
		byte[] key;
		try
		{
			key = Convert.FromBase64String(value);
		}
		catch
		{
			return Result.Failure<Ed25519PublicKey>(DomainErrors.Ed25519PublicKey.IsNotWellFormedBase64);
		}

		return key.Length == ValueLength
			? new Ed25519PublicKey(value)
			: Result.Failure<Ed25519PublicKey>(DomainErrors.Ed25519PublicKey.HasInvalidKeyLength);
	}

	protected override IEnumerable<object> GetAtomicValues()
	{
		yield return Value;
	}
}
