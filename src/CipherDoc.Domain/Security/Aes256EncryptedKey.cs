using CipherDoc.Domain.Core;
using CipherDoc.Domain.Core.Primitives;
using CipherDoc.Domain.Core.Primitives.Result;

namespace CipherDoc.Domain.Security;

public class Aes256EncryptedKey : ValueObject
{
	private const int NonceLength = 24;
	private const int ValueLength = 272;
	
	public string Value { get; }

	public string Nonce { get; }

	private Aes256EncryptedKey(string value, string nonce)
	{
		Value = value;
		Nonce = nonce;
	}

	public static Result<Aes256EncryptedKey> Create(string base64Value, string base64Nonce)
	{
		if (string.IsNullOrEmpty(base64Value) || string.IsNullOrEmpty(base64Nonce))
		{
			return Result.Failure<Aes256EncryptedKey>(DomainErrors.Generic.IsNullOrEmpty);
		}

		var isValidValue = IsValidValue(base64Value);
		var isValidNonce = IsValidNonce(base64Nonce);

		if (isValidValue.IsFailure)
		{
			return Result.Failure<Aes256EncryptedKey>(isValidValue.Error);
		}

		if (isValidNonce.IsFailure)
		{
			return Result.Failure<Aes256EncryptedKey>(isValidNonce.Error);
		}

		return new Aes256EncryptedKey(base64Value, base64Nonce);
	}

	private static Result IsValidValue(string base64Value)
		=> AreValidBase64Bytes(base64Value, ValueLength);

	private static Result IsValidNonce(string base64Nonce)
		=> AreValidBase64Bytes(base64Nonce, NonceLength);

	private static Result AreValidBase64Bytes(string encoded, int length)
	{
		byte[] decoded;
		try
		{
			decoded = Convert.FromBase64String(encoded);
		}
		catch
		{
			return Result.Failure(DomainErrors.Aes256EncryptedKey.IsNotWellFormedBase64);
		}

		if (decoded.Length != length)
		{
			return Result.Failure(DomainErrors.Aes256EncryptedKey.HasInvalidKeyLength);
		}

		return Result.Success();
	}

	protected override IEnumerable<object> GetAtomicValues()
	{
		yield return Value;
		yield return Nonce;
	}
}
