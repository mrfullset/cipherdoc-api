using CipherDoc.Domain.Core.Primitives;
using System.Security.Cryptography;
using CipherDoc.Domain.Core;
using CipherDoc.Domain.Core.Primitives.Result;

namespace CipherDoc.Domain.Security;

public class Pbkdf2Hash : ValueObject
{
	private const int SaltLength = 128;
	private const int HashIterationCount = 600_000;
	private const int HashOutputLength = 256;
	private const int ClientHashLength = 64;

	public string Value { get; }

	public string Salt { get; }

	private Pbkdf2Hash(string value, string salt)
	{
		Value = value;
		Salt = salt;
	}

	public static Result<Pbkdf2Hash> Create(string clientHash)
	{
		var clientHashValidationResult = IsClientHashValid(clientHash);
		if (clientHashValidationResult.IsFailure)
		{
			return Result.Failure<Pbkdf2Hash>(clientHashValidationResult.Error);
		}

		var salt = GenerateSalt();
		var value = Hash(clientHash, salt);

		return new Pbkdf2Hash(Convert.ToBase64String(value),
			Convert.ToBase64String(salt));
	}

	public bool EqualsToClientHash(string clientHash)
	{
		var value = Hash(clientHash, Convert.FromBase64String(Salt));
		return Convert.ToBase64String(value) == Value;
	}

	private static byte[] GenerateSalt()
		=> RandomNumberGenerator.GetBytes(SaltLength);

	private static byte[] Hash(string value, byte[] salt)
		=> Rfc2898DeriveBytes.Pbkdf2(value, salt, HashIterationCount, HashAlgorithmName.SHA256, HashOutputLength);

	private static Result IsClientHashValid(string value)
	{
		if (string.IsNullOrEmpty(value))
		{
			return Result.Failure(DomainErrors.Generic.IsNullOrEmpty);
		}

		if (value.Length != ClientHashLength)
		{
			return Result.Failure(DomainErrors.Pbkdf2Hash.HasInvalidHashLength);
		}

		return Result.Success();
	}

	protected override IEnumerable<object> GetAtomicValues()
	{
		yield return Value;
		yield return Salt;
	}
}
