using CipherDoc.Domain.Core.Primitives;

namespace CipherDoc.Domain.Security;

public sealed class EncryptedDocumentKey : Entity
{
	public Aes256EncryptedKey Key { get; private set; }

	public PublicKey EncryptionKey { get; private set; }

	private EncryptedDocumentKey() {}

	private EncryptedDocumentKey(Aes256EncryptedKey key, PublicKey encryptionKey)
	{
		Key = key;
		EncryptionKey = encryptionKey;
	}

	public static EncryptedDocumentKey Create(Aes256EncryptedKey key, PublicKey encryptionKey)
	{
		return new EncryptedDocumentKey(key, encryptionKey);
	}
}
