using System.Security.Cryptography;
using CipherDoc.Domain.Core;
using CipherDoc.Domain.Core.Primitives;
using CipherDoc.Domain.Core.Primitives.Result;

namespace CipherDoc.Domain.Security;

public sealed class ActivationSalt : ValueObject
{
	private const int SaltLength = 128;

	public string Value { get; }

	private ActivationSalt(string value)
	{
		Value = value;
	}

	public static Result<ActivationSalt> Create(string value)
	{
		if (string.IsNullOrEmpty(value))
		{
			return Result.Failure<ActivationSalt>(DomainErrors.Generic.IsNullOrEmpty);
		}

		return ParseBase64Salt(value);
	}

	public static ActivationSalt Create()
	{
		var salt = Generate();
		return new ActivationSalt(Convert.ToBase64String(salt));
	}

	private static Result<ActivationSalt> ParseBase64Salt(string value)
	{
		byte[] salt;
		try
		{
			salt = Convert.FromBase64String(value);
		}
		catch
		{
			return Result.Failure<ActivationSalt>(DomainErrors.ActivationSalt.IsNotWellFormedBase64);
		}

		return salt.Length == SaltLength
			? new ActivationSalt(value)
			: Result.Failure<ActivationSalt>(DomainErrors.ActivationSalt.HasInvalidKeyLength);
	}

	private static byte[] Generate()
		=> RandomNumberGenerator.GetBytes(SaltLength);

	protected override IEnumerable<object> GetAtomicValues()
	{
		yield return Value;
	}
}
