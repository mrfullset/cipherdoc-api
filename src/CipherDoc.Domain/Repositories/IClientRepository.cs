using CipherDoc.Domain.User;

namespace CipherDoc.Domain.Repositories;

public interface IClientRepository
{
	Task<bool> IsLoginUnique(string login);

	Task<Client?> GetById(int id);

	Task<Client?> GetByLogin(string login, CancellationToken cancellationToken = default);

	void Insert(Client user);
}
