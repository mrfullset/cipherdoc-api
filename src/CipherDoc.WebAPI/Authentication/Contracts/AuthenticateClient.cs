namespace CipherDoc.WebAPI.Authentication.Contracts;

public record AuthenticateClientRequest(string Login, string PasswordHash);

public record AuthenticateClientResponse(string Token);
