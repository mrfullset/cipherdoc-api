using CipherDoc.Application.Clients.Commands;
using CipherDoc.WebAPI.Authentication.Contracts;
using Mapster;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace CipherDoc.WebAPI.Authentication;

[Route("auth")]
[Produces("application/json")]
public class AuthenticationController : Controller
{
	private readonly IMediator _mediator;

	public AuthenticationController(IMediator mediator)
	{
		_mediator = mediator;
	}

	[HttpPost("client/token")]
	[ProducesResponseType(StatusCodes.Status200OK)]
	[ProducesResponseType(StatusCodes.Status401Unauthorized)]
	public async Task<ActionResult<AuthenticateClientResponse>> AuthenticateClient([FromBody] AuthenticateClientRequest request)
	{
		var tokenResult = await _mediator.Send(request.Adapt<LoginClientCommand>());
		if (tokenResult.IsFailure)
		{
			return Unauthorized(tokenResult.Error);
		}
		var response = new AuthenticateClientResponse(tokenResult.Value);
		return Ok(response);
	}
}
