using System.Reflection;

namespace CipherDoc.WebAPI;

public static class AssemblyReference
{
	public static readonly Assembly Assembly = typeof(AssemblyReference).Assembly;
}
