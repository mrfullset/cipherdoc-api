using CipherDoc.Application.Test;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace CipherDoc.WebAPI.Controllers;

[Route("test")]
public class TestController
{
	private readonly IMediator _mediator;

	public TestController(IMediator mediator)
	{
		_mediator = mediator;
	}

	[HttpGet]
	public string Get()
	{
		_mediator.Send(new TestCommand());
		return "OK";
	}
}
