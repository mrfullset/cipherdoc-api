using CipherDoc.Domain.Core.Primitives;

namespace CipherDoc.Application.Core;

public static class ApplicationErrors
{
	public static class Authentication
	{
		public static readonly Error InvalidCredentials
			= new Error(nameof(Authentication) + nameof(InvalidCredentials), "Invalid credentials");
	}
}
