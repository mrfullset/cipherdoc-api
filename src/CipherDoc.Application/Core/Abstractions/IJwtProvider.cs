using CipherDoc.Domain.User;

namespace CipherDoc.Application.Core.Abstractions;

public interface IJwtProvider
{
	public string Generate(Client client);
}
