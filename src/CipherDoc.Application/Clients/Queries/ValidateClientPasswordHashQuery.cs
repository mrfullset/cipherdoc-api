using CipherDoc.Domain.Repositories;
using CipherDoc.Domain.User;
using MediatR;

namespace CipherDoc.Application.Clients.Queries;

public sealed record ValidateClientPasswordHashQuery(Client Client, string PasswordHash) : IRequest<bool>;

internal sealed class ValidateClientPasswordHashQueryHandler : IRequestHandler<ValidateClientPasswordHashQuery, bool>
{
	public async Task<bool> Handle(ValidateClientPasswordHashQuery request, CancellationToken cancellationToken)
		=> request.Client.IsValidPasswordHash(request.PasswordHash);
}
