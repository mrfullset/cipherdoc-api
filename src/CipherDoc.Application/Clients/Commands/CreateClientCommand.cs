using CipherDoc.Application.Core.Abstractions.Data;
using CipherDoc.Domain.Core;
using CipherDoc.Domain.Core.Primitives.Result;
using CipherDoc.Domain.Repositories;
using CipherDoc.Domain.Security;
using CipherDoc.Domain.User;
using MediatR;

namespace CipherDoc.Application.Clients.Commands;

public sealed record CreateClientCommand(string Login, string PasswordHash, string PublicKey, string ActivationSalt) : IRequest<Result<Client>>;

internal sealed class CreateClientCommandHandler : IRequestHandler<CreateClientCommand, Result<Client>>
{
	private readonly IClientRepository _clientRepository;

	private readonly IUnitOfWork _unitOfWork;


	public CreateClientCommandHandler(IClientRepository clientRepository, IUnitOfWork unitOfWork)
	{
		_clientRepository = clientRepository;
		_unitOfWork = unitOfWork;
	}

	public async Task<Result<Client>> Handle(CreateClientCommand request, CancellationToken cancellationToken)
	{
		var isLoginUnique = await _clientRepository.IsLoginUnique(request.Login);
		if (!isLoginUnique)
		{
			return Result.Failure<Client>(DomainErrors.User.LoginAlreadyExists);
		}

		var publicKeyResult = GetPublicKey(request.PublicKey, request.ActivationSalt);
		if (publicKeyResult.IsFailure)
		{
			return Result.Failure<Client>(publicKeyResult.Error);
		}

		var hashedPasswordResult = Pbkdf2Hash.Create(request.PasswordHash);
		if (hashedPasswordResult.IsFailure)
		{
			return Result.Failure<Client>(hashedPasswordResult.Error);
		}

		var client = Client.Create(request.Login, hashedPasswordResult.Value, publicKeyResult.Value);

		_clientRepository.Insert(client);

		await _unitOfWork.SaveChangesAsync(cancellationToken);

		return client;
	}

	private static Result<PublicKey> GetPublicKey(string publicKey, string activationSalt)
	{
		var ed25519PublicKeyResult = Ed25519PublicKey.Create(publicKey);
		if (ed25519PublicKeyResult.IsFailure)
		{
			return Result.Failure<PublicKey>(ed25519PublicKeyResult.Error);
		}

		var activationSaltResult = ActivationSalt.Create(activationSalt);
		if (activationSaltResult.IsFailure)
		{
			return Result.Failure<PublicKey>(activationSaltResult.Error);
		}

		return PublicKey.Create(ed25519PublicKeyResult.Value, activationSaltResult.Value);
	}

}
