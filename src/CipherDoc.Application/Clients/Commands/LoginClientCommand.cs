using CipherDoc.Application.Core;
using CipherDoc.Application.Core.Abstractions;
using CipherDoc.Domain.Core.Primitives.Result;
using CipherDoc.Domain.Repositories;
using MediatR;

namespace CipherDoc.Application.Clients.Commands;

public record LoginClientCommand(string Login, string PasswordHash) : IRequest<Result<string>>;

internal sealed class LoginClientCommandHandler : IRequestHandler<LoginClientCommand, Result<string>>
{
	private readonly IClientRepository _clientRepository;

	private readonly IJwtProvider _jwtProvider;

	public LoginClientCommandHandler(IClientRepository clientRepository, IJwtProvider jwtProvider)
	{
		_clientRepository = clientRepository;
		_jwtProvider = jwtProvider;
	}

	public async Task<Result<string>> Handle(LoginClientCommand request, CancellationToken cancellationToken)
	{
		var client = await _clientRepository.GetByLogin(request.Login, cancellationToken);
		var isPasswordValid = client?.IsValidPasswordHash(request.PasswordHash) ?? false;
		if (client is null || !isPasswordValid)
		{
			return Result.Failure<string>(ApplicationErrors.Authentication.InvalidCredentials);
		}

		return _jwtProvider.Generate(client);
	}
}
