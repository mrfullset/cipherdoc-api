using MediatR;

namespace CipherDoc.Application.Test;

public record TestCommand() : IRequest;
