using MediatR;

namespace CipherDoc.Application.Test;

public class TestCommandHandler : IRequestHandler<TestCommand>
{
	public async Task Handle(TestCommand request, CancellationToken cancellationToken)
	{
		await Task.Run(() => Console.WriteLine("TEST OK"), cancellationToken);
	}
}
