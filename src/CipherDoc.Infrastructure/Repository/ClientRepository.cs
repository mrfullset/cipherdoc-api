using CipherDoc.Application.Core.Abstractions.Data;
using CipherDoc.Domain.Repositories;
using CipherDoc.Domain.User;
using Microsoft.EntityFrameworkCore;

namespace CipherDoc.Infrastructure.Repository;

internal sealed class ClientRepository : GenericRepository<Client>, IClientRepository
{
	public ClientRepository(IDbContext dbContext) : base(dbContext)
	{
	}

	public async Task<bool> IsLoginUnique(string login)
		=> !await DbContext.Set<Client>()
			.AnyAsync(client => client.Login == login);

	public async Task<Client?> GetById(int id) => await GetByIdAsync(id);

	public async Task<Client?> GetByLogin(string login, CancellationToken cancellationToken = default)
		=> await Set().FirstOrDefaultAsync(client => client.Login == login, cancellationToken);
}
