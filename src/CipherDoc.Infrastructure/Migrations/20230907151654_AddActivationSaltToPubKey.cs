﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CipherDoc.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class AddActivationSaltToPubKey : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ActivationSalt",
                table: "PublicKey",
                type: "text",
                nullable: false);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ActivationSalt",
                table: "PublicKey");
        }
    }
}
