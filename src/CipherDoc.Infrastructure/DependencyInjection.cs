using CipherDoc.Application.Core.Abstractions.Data;
using CipherDoc.Domain.Repositories;
using CipherDoc.Infrastructure.Persistence;
using CipherDoc.Infrastructure.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace CipherDoc.Infrastructure;

public static class DependencyInjection
{
	public static IServiceCollection AddPersistence(this IServiceCollection services)
	{
		services.AddDbContext<ApplicationDbContext>(c
			=> c.UseNpgsql("Server=localhost;Port=15432;"
				+ "Userid=cipherdoc;Password=cipherdoc;SslMode=Disable;"
				+ "Database=cipherdoc"));

		services.AddScoped<IDbContext>(serviceProvider => serviceProvider.GetRequiredService<ApplicationDbContext>());

		services.AddScoped<IUnitOfWork>(serviceProvider => serviceProvider.GetRequiredService<ApplicationDbContext>());

		services.AddScoped<IClientRepository, ClientRepository>();

		return services;
	}
}
