using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using CipherDoc.Application.Core.Abstractions;
using CipherDoc.Domain.User;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace CipherDoc.Infrastructure.Authentication;

internal sealed class JwtProvider : IJwtProvider
{
	private readonly SigningCredentials _signingCredentials;
	private readonly JwtSecurityTokenHandler _tokenHandler;
	private readonly JwtOptions _jwtOptions;

	public JwtProvider(IOptions<JwtOptions> jwtOptions)
	{
		_jwtOptions = jwtOptions.Value;

		// todo: asymmetric key (Ed25519 ?)
		_signingCredentials = new(
			new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtOptions.SecretKey)),
			SecurityAlgorithms.HmacSha256);

		_tokenHandler = new JwtSecurityTokenHandler();
	}

	public string Generate(Client client)
	{
		// todo: hash user ids
		var claims = new Claim[]
		{
			new Claim(JwtRegisteredClaimNames.Sub, client.Id.ToString()), new Claim(JwtRegisteredClaimNames.Email, client.Login)
		};

		var token = new JwtSecurityToken(_jwtOptions.Issuer,
			_jwtOptions.Audience,
			claims,
			DateTime.Now,
			DateTime.Now.AddHours(1),
			_signingCredentials);

		return _tokenHandler.WriteToken(token);
	}
}
