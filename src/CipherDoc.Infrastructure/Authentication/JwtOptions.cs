namespace CipherDoc.Infrastructure.Authentication;

internal sealed class JwtOptions
{
	public string Issuer { get; init; } = null!;

	public string Audience { get; init; } = null!;

	public string SecretKey { get; init; } = null!;
}
