using System.Text;
using CipherDoc.Application.Core.Abstractions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace CipherDoc.Infrastructure.Authentication;

public static class AuthenticationConfiguration
{
	public static IServiceCollection AddCipherDocAuthentication(this IServiceCollection services,
		IConfiguration configuration)
	{
		services.AddJwtOptions(configuration);

		services.AddScoped<IJwtProvider, JwtProvider>();

		services.AddJwtBearerAuthentication(configuration);

		return services;
	}

	private static IServiceCollection AddJwtOptions(this IServiceCollection services,
		IConfiguration configuration)
	{
		var section = GetJwtSection(configuration);

		services.Configure<JwtOptions>(section);

		return services;
	}

	private static IServiceCollection AddJwtBearerAuthentication(this IServiceCollection services,
		IConfiguration configuration)
	{
		var jwtOptions = GetJwtSection(configuration).Get<JwtOptions>();

		services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
			.AddJwtBearer(options =>
			{
				options.TokenValidationParameters = new TokenValidationParameters()
				{
					ValidateAudience = true,
					ValidAudience = jwtOptions!.Audience,

					ValidateIssuer = true,
					ValidIssuer = jwtOptions.Issuer,

					ValidateIssuerSigningKey = true,
					IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtOptions.SecretKey))
				};
			});

		return services;
	}

	private static IConfigurationSection GetJwtSection(IConfiguration configuration)
	{
		var section = configuration.GetSection("Jwt");
		if (section is null)
		{
			throw new ArgumentException("Jwt section is not defined in configuration");
		}

		return section;
	}
}
