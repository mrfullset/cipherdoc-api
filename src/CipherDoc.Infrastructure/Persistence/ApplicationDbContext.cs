using CipherDoc.Application.Core.Abstractions.Data;
using CipherDoc.Domain.Core.Primitives;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace CipherDoc.Infrastructure.Persistence;

internal sealed class ApplicationDbContext : DbContext, IUnitOfWork, IDbContext
{
	private readonly IMediator _mediator;

	public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, IMediator mediator) : base(options)
	{
		_mediator = mediator;
	}

	protected override void OnModelCreating(ModelBuilder modelBuilder)
	{
		modelBuilder.ApplyConfigurationsFromAssembly(typeof(ApplicationDbContext).Assembly);
	}

	public Task<IDbContextTransaction> BeginTransactionAsync(CancellationToken cancellationToken = default)
		=> Database.BeginTransactionAsync(cancellationToken);

	public new DbSet<TEntity> Set<TEntity>() where TEntity : Entity
		=> base.Set<TEntity>();

	public async Task<TEntity?> GetBydIdAsync<TEntity>(int id) where TEntity : Entity
	{
		if (id == default)
		{
			return null;
		}
		return await Set<TEntity>().FirstOrDefaultAsync(e => e.Id == id);
	}

	public void Insert<TEntity>(TEntity entity) where TEntity : Entity
		=> Set<TEntity>().Add(entity);

	public void InsertRange<TEntity>(IReadOnlyCollection<TEntity> entities) where TEntity : Entity
		=> Set<TEntity>().AddRange(entities);

	public new void Remove<TEntity>(TEntity entity) where TEntity : Entity
		=> Set<TEntity>().Remove(entity);

	public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
	{
		await PublishDomainEvents(cancellationToken);
		return await base.SaveChangesAsync(cancellationToken);
	}

	private async Task PublishDomainEvents(CancellationToken cancellationToken)
	{
		var aggregateRoots = ChangeTracker
			.Entries<AggregateRoot>()
			.Where(entityEntry => entityEntry.Entity.DomainEvents.Any())
			.ToList();

		var domainEvents = aggregateRoots
			.SelectMany(entityEntry => entityEntry.Entity.DomainEvents)
			.ToList();

		aggregateRoots
			.ForEach(entityEntry => entityEntry.Entity.ClearDomainEvents());

		var tasks = domainEvents
			.Select(domainEvent => _mediator.Publish(domainEvent, cancellationToken));

		await Task.WhenAll(tasks);
	}
}
