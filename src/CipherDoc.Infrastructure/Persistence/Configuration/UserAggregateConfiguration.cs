using CipherDoc.Domain.Security;
using CipherDoc.Domain.User;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CipherDoc.Infrastructure.Persistence.Configuration;

public class UserAggregateConfiguration : IEntityTypeConfiguration<User>, IEntityTypeConfiguration<Client>, IEntityTypeConfiguration<PublicKey>
{
	public void Configure(EntityTypeBuilder<User> builder)
	{
		builder.HasKey(user => user.Id);

		builder.Property(user => user.Login);
	}

	public void Configure(EntityTypeBuilder<Client> builder)
	{
		builder.ToTable("Clients");

		builder
			.OwnsOne<Pbkdf2Hash>("_hashedPassword", navigationBuilder =>
			{
				navigationBuilder
					.Property(hashedPassword => hashedPassword.Value)
					.HasColumnName("HashedPassword")
					.IsRequired();
				navigationBuilder
					.Property(hashedPassword => hashedPassword.Salt)
					.HasColumnName("Salt")
					.IsRequired();
			})
			.Navigation("_hashedPassword")
			.IsRequired();

		builder
			.Metadata
			.FindNavigation(nameof(Client.Folders))!
			.SetField("_folders");
	}

	public void Configure(EntityTypeBuilder<PublicKey> builder)
	{
		builder.HasKey(key => key.Id);

		builder
			.Property(key => key.Key)
			.HasConversion(key => key.Value,
				value => Ed25519PublicKey.Create(value).Value);

		builder
			.Property(key => key.ActivationSalt)
			.HasConversion(salt => salt.Value,
				value => ActivationSalt.Create(value).Value);

		builder
			.HasOne<User>()
			.WithOne(user => user.PublicKey)
			.HasForeignKey<PublicKey>("UserId")
			.IsRequired();
	}
}
