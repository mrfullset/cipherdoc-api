using CipherDoc.Domain.Resource;
using CipherDoc.Domain.Security;
using CipherDoc.Domain.User;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CipherDoc.Infrastructure.Persistence.Configuration;

public class ResourceAggregateConfiguration : IEntityTypeConfiguration<Resource>, IEntityTypeConfiguration<Document>, IEntityTypeConfiguration<Folder>, IEntityTypeConfiguration<EncryptedDocumentKey>
{
	public void Configure(EntityTypeBuilder<Resource> builder)
	{
		builder.HasKey(resource => resource.Id);

		builder.Property(resource => resource.Name);
	}

	public void Configure(EntityTypeBuilder<Document> builder)
	{
		builder.ToTable("Documents");

		builder.Property(document => document.FileName);

		builder
			.HasOne(document => document.Key)
			.WithOne()
			.HasForeignKey<EncryptedDocumentKey>("DocumentId")
			.IsRequired();
	}

	public void Configure(EntityTypeBuilder<Folder> builder)
	{
		builder.ToTable("Folders");

		builder
			.HasOne<Client>()
			.WithMany(client => client.Folders)
			.HasForeignKey("OwnerId")
			.IsRequired();

		builder
			.HasMany(folder => folder.Documents)
			.WithOne()
			.HasForeignKey("FolderId");
		builder
			.Metadata
			.FindNavigation(nameof(Folder.Documents))!
			.SetField("_documents");
	}

	public void Configure(EntityTypeBuilder<EncryptedDocumentKey> builder)
	{
		builder.HasKey(key => key.Id);

		builder.HasOne(key => key.EncryptionKey);

		builder.OwnsOne(key => key.Key, navigationBuilder =>
		{
			navigationBuilder
				.Property(encryptedKey => encryptedKey.Value)
				.HasColumnName("Value");
			navigationBuilder
				.Property(encryptedKey => encryptedKey.Nonce)
				.HasColumnName("Nonce");
		});
	}
}
